import { LitExtended } from 'lit-extended';
import { isRelative, isAbsolute, urlAsAbsolutePath } from 'lit-extended/paths';

export class LitDependency extends LitExtended {
	static get properties() {
		return {
			component: {
				type: String,
				attribute: true
			}
		}
	}

	firstUpdated() {
		if (!this.component) throw new Error('You must include a component name');

		if (!!this.component) {
			if (!isAbsolute(this.component) && !isRelative(this.component))
				throw new Error(`Failed to resolve module specifier "${this.component}". Relative references must start with either "/", "./", or "../".`);

			import(urlAsAbsolutePath(this.component))
				.catch((e) => console.error(e));
		}
	}

	render() { /* no template */ }
}

customElements.define('lit-dependency', LitDependency);