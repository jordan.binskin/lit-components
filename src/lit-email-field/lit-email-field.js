import { LitTextField } from '../lit-text-field/lit-text-field';

export class LitEmailField extends LitTextField {
	get __inputType() {
		return 'email';
	}
	
	__validate() {
		super.__validate();

		if (!this.isValid) return;

		let emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		let isEmailFormat = emailPattern.test(this.__renderedField.value.toLowerCase());

		if (!isEmailFormat) this.isValid = false;
	}
}

customElements.define('lit-email-field', LitEmailField);
