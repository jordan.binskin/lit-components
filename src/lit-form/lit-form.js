
import { css, LitExtended } from 'lit-extended';

class LitForm extends LitExtended {
	constructor() {
		super();
		this.fields = [];
		this.requestOptions = {};

		this.addEventListener('submit', () => this.__trySubmit());
	}


	static get properties() {
		return {
			mimeType: {
				type: String,
				attribute: 'mime-type'
			},
			targetURL: {
				type: String,
				attribute: 'target-url',
				reflect: true
			}
		}
	}


	get childInputs() {
		return Array.from(this.querySelectorAll('form-select-field, form-input-field, form-email-field'));
	}


	get validInputs() {
		return this.childInputs.filter(field => field.isValid)
	}


	get invalidInputs() {
		return this.childInputs.filter(field => field.required && !field.isValid)
	}


	get __encodedValues() {

		let dataObj = this.childInputs
			.reduce((result, item) => {
				result[item.key] = item.value;
				return result;
			}, {});

		if (this.mimeType == 'application/json') {
			return JSON.stringify(dataObj);
		}

		if (this.mimeType == 'x-www-form-urlencoded') {
			let params = new URLSearchParams(dataObj)
			return params.toString()
		}

		throw new TypeError('unsupported form mime type')
	}


	__trySubmit() {
		this.childInputs.forEach(field => {
			field.emit('submitted')
		})
		if (this.invalidInputs.length <= 0) this.__postValues(this.validInputs);
	}


	__postValues() {
		let request = new Request(this.targetURL, {
			method: 'POST',
			headers: {
				'Content-Type': this.mimeType,
			},
			body: this.__encodedValues
		});

		fetch(request)
			.then(resp => {
				if (resp.status < 200 || resp.status > 299)
					throw new Error(`Response was ${resp.status}`);
				
				if (!resp.headers.get('content-type')) throw new Error('Test');

				return resp.json();
			})
			.then(response => this.emit('success', response))
			.catch(err => this.emit('error', err));
	}

	static get styles() {
		return css`
			::slotted(lit-button) {
				margin-top: 2.25rem;
			}

			@media screen and (min-width: 768px) {
				::slotted(lit-button) {
					margin-left: var(--label-size, 9rem);
					margin-top: 0;
				}
			}
		`
	}
}

customElements.define('lit-form', LitForm);