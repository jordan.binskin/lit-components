import { html, css, LitExtended, classMap, styleMap } from 'lit-extended';

export class LitCollapse extends LitExtended {
	firstUpdated() {
		this.__outer = this.query('.outer');
		this.__inner = this.query('.inner');
	}

	static get properties() {
		return {
			animating: {
				type: Boolean,
				attribute: true,
				reflect: true
			},
			open: {
				type: Boolean,
				attribute: true,
				reflect: true
			}
		}
	}

	get innerHeight() {
		if (!this.__inner) return 0;
		return this.__inner.offsetHeight;
	}

	get __appliedClasses() {
		return classMap({
			'outer': true,
			'open': this.__open,
			'animating': this.animating
		});
	}

	get __appliedStyles() {
		let height = this.open? `${this.innerHeight}px` : 0;
		if (this.open && !this.animating) height = 'auto';

		return styleMap({ height });
	}

	get __animationEnd() {
		return () => {
			this.animating = false;
		}
	}

	get open() { return this.__open || false }
	set open(val) {
		if (this.innerHeight < 1) {
			this.__open = val;
			this.requestUpdate('open');
			return;
		}

		this.animating = true;
		this.updateComplete
			.then(() => {
				if (val === false) {
					this.__open = true;
					this.requestUpdate('open');
					return this.updateComplete;
				}
			})
			.then(() => requestAnimationFrame(() => {
				this.__open = val;
				this.__outer.addEventListener('transitionend', this.__animationEnd);
				this.requestUpdate('open');
			}))

			.catch(() => { /* noop */ });
	}

	static get styles() {
		return css`
			:host {
				display: block;
			}

			.inner, .outer {
				overflow: hidden;
			}

			.outer {
				mask: 0 0/ 0 100%;
			}

			.outer.open {
				mask: 0 0/ 100% 100%;
			}

			.animating {
				transition: 0.5s ease-in-out;
			}
		`;
	}

	render() {
		return html`
			<div class="${this.__appliedClasses}" style="${this.__appliedStyles}">
				<div class="inner">
					<slot></slot>
				</div>
			</div>
		`;
	}
}

customElements.define('lit-collapse', LitCollapse);