import { LitExtended, render } from 'lit-extended';

export class LitPage extends LitExtended {
	static get properties() {
		return {
			route: {
				type: String,
				attribute: true
			},
			href: {
				type: String,
				attribute: true
			}
		}
	}

	render() { /* no template */ }
}

customElements.define('lit-page', LitPage);