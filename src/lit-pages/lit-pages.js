import { LitExtended, render } from 'lit-extended';
import { isAbsolute } from 'lit-extended/paths';

import './lit-page';

const obj = {};
const pages = {
	get set() {
		return (key, value) => {
			if (obj[key]) return;
			obj[key] = value;
		}
	},

	get get() {
		return (key) => {
			return obj[key];
		}
	},

	get has() {
		return (key) => {
			return obj[key] != undefined;
		}
	}
};

let routers = [];
window.addEventListener('popstate', e => {
	routers.forEach(router => router.changeRoute(e.target.location.pathname));
});

LitExtended.extentions.set('lit-pages', () => {
	return routers.map(router => 
		router.routes.map(route => 
			`${route.route} -> ${route.href}`
		)
	)
});

export class LitPages extends LitExtended {
	constructor() {
		super();

		routers.push(this);

		this.shadowRoot.appendChild(document.createElement('slot'));
		this.__children = Array.from(this.children);

		this.base = '/fragments/';
		this.addEventListener('click', e => this.__handleRoute(e));
	}

	static get properties() {
		return {
			base: {
				type: String,
				attribute: true
			}
		}
	}

	get routes() {
		return this.__children.filter(child => child.tagName.toLowerCase() == 'lit-page');
	}

	async loadPage(url) {
		return pages.get(url) || await fetch(url);
	}

	__handleRoute(e) {
		e.preventDefault();

		if (e.target.tagName.toLowerCase() !== 'a') return;

		// console.log('Handle Route', e.target.pathname);
		this.changeRoute(e.target.pathname);
	}

	firstUpdated() {
		this.changeRoute(location.pathname);
	}

	getMatchingRoutes(routePath) {
		return this.routes.filter(route => route.route == routePath);
	}

	changeRoute(routePath) {
		let matchedRoutes = this.getMatchingRoutes(routePath);

		if (!matchedRoutes.length) return console.warn(`No routes matches '${routePath}'`);

		let firstMatch = matchedRoutes[0];

		let requestPath = firstMatch.href;
		if (!isAbsolute(requestPath)) requestPath = this.base + requestPath;

		this
			.loadPage(requestPath)
			.then(resp => {
				if (resp.status < 200 || resp.status > 299)
					throw new Error(`Request for '${requestPath}' got error '${resp.status}'`);

				return resp.text();
			})

			.then(content => render(eval(`html\`${content}\``), this))

			.then(() => {
				history.pushState({}, '', routePath);
			})
	}

	render() { /* no template */ }
}

customElements.define('lit-pages', LitPages);