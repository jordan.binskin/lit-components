import { css, LitExtended } from 'lit-extended';

export class LitGrid extends LitExtended {
	static get styles() {
		return css`
			:host {
				display: flex;
				flex-direction: row;
				margin-left: -1rem;
				margin-right: -1rem;
			}

			::slotted(*) {
				--span: 12;
				width: calc((100% / 12) * var(--span));
				box-sizing: border-box;
				padding: 0 1rem;
			}

			::slotted(.span-1)	{ --span:	1 }
			::slotted(.span-2)	{ --span:	2 }
			::slotted(.span-3)	{ --span:	3 }
			::slotted(.span-4)	{ --span:	4 }
			::slotted(.span-5)	{ --span:	5 }
			::slotted(.span-6)	{ --span:	6 }
			::slotted(.span-7)	{ --span:	7 }
			::slotted(.span-8)	{ --span:	8 }
			::slotted(.span-9)	{ --span:	9 }
			::slotted(.span-10)	{ --span:	10 }
			::slotted(.span-11)	{ --span:	11 }
			::slotted(.span-12)	{ --span:	12 }
		`;
	}
}

customElements.define('lit-grid', LitGrid);