import { html, css, LitExtended } from 'lit-extended';

export class LitSection extends LitExtended {
	static get styles() {
		return css`
			:host {
				display: block;
				overflow: hidden;
				box-sizing: border-box;
			}

			.wrapper {
				overflow: hidden;
				box-sizing: border-box;

				max-width: var(--max-column, 320px);
				min-width: 320px;

				margin: 0 auto;
				padding: var(--section-padding, 2rem) var(--column-padding, 1rem);
			}

			@media screen and (min-width: 550px) {
				:host {
					--max-column: 550px;
					--column-padding: 2rem;
				}
			}

			@media screen and (min-width: 768px) {
				:host {
					--max-column: 768px;
					--column-padding: 3rem;
				}
			}

			@media screen and (min-width: 960px) {
				:host {
					--max-column: 960px;
					--column-padding: 4rem;
				}
			}

			@media screen and (min-width: 1200px) {
				:host { --max-column: 1200px }
			}

			@media screen and (min-width: 1600px) {
				:host { --max-column: 1600px }
			}
		`;
	}

	render() {
		return html`
			<section class="wrapper">
				<slot></slot>
			</section>
		`;
	}
}

customElements.define('lit-section', LitSection);