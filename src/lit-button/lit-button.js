import { html, css, LitExtended, ifDefined } from 'lit-extended';

export class LitButton extends LitExtended {
	static get properties() {
		return {
			href: {
				type: String,
				attribute: true
			}
		}
	}

	get __isBlank() {
		if (!this.href) return false;
		if (this.href.replace(/http(s)?\:\/\/?/i, '').indexOf(window.location.hostname) == 0) return true;
		return false;
	}


	static get styles() {
		return css`
			:host {
				--padding-v: 1rem;
				--padding-h: 2rem;

				display: var(--display-mode, flex);
				justify-content: center;

				min-width: 180px;

				padding-left: var(--padding-h, 2rem);
				padding-right: var(--padding-h, 2rem);
				
				box-shadow: 0 0 3px -2px #0006 inset;
				background-color: #33f;
				
				color: #fff;
				font-weight: 700;
				letter-spacing: .025rem;
				
				box-sizing: border-box;
				border-radius: 3px;
			}

			a {
				display: inline-flex;
				padding-top: var(--padding-v, 1rem);
				padding-bottom: var(--padding-v, 1rem);
				color: inherit;
			}

			@media screen and (min-width: 768px) {
				:host {
					--display-mode: inline-flex;
				}
			}
		`
	}

	render() {
		return html`
			<a href=${ifDefined(this.href)} target=${this.__isBlank? '_blank': '_self'}>
				<slot></slot>
			</a>
		`
	}
}

customElements.define('lit-button', LitButton);