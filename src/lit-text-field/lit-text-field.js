
import { html, css, LitExtended, classMap } from 'lit-extended';

export class LitTextField extends LitExtended {
	constructor() {
		super();

		this.__validate();
		this.isPristine = true;

		this.addEventListener('submitted', () => this.__validate());
		
		this.addEventListener('blur', () => this.__validate());
		this.addEventListener('keyup', () => this.__validate());
	}

	static get properties() {
		return {
			type: {
				type: String,
				reflect: true
			},
			label: {
				type: String,
				attribute: true
			},
			name: {
				type: String,
				attribute: true
			},

			isPassword: {
				type: Boolean,
				attribute: 'is-password'
			},

			isRequired: {
				type: Boolean,
				attribute: 'required',
				reflect: true
			},
			value: {
				type: String,
				attribute: true,
				reflect: true
			},

			isPristine: {
				type: Boolean,
				attribute: 'is-pristine',
				reflect: true
			},
			isValid: {
				type: Boolean,
				reflect: true
			}
		}
	}

	get __hasValue() {
		if (!this.__renderedField) return false;
		return !!this.__renderedField.value && this.__renderedField.value.trim() !== ''
	}

	get __shouldValidate() {
		return !this.isPristine && this.isRequired;
	}

	__validate() {
		if (!this.__renderedField) return;
		let valid = true;
		if (this.isRequired && (!this.__hasValue)) valid = false;

		//	commit validation
		this.isValid = valid;
		this.isPristine = false;
	}


	get __renderedField() {
		return this.query('input');
	}

	get __fieldClasses() {
		return classMap({
			field: true,
			error: this.__shouldValidate && !this.isValid,
			pristine: this.isPristine,
			valid: this.__shouldValidate && this.isValid
		});
	}

	get __fieldType() {
		return this.isPassword?
			'password': 'text';
	}


	static get styles() {
		return css`
			:host {
				--border: #0006;
				--background: #00000003;
			
				margin-bottom:10px;
				display:block;
				color:#000;

				margin: 1rem 0;
			}

			.row {
				display: flex;
				flex-direction: var(--flex-dir, column);
			}
			
			.label {
				display: flex;
				align-items: center;

				justify-content: var(--flex-algin, flex-start);
				text-align: right;

				min-width: calc(var(--rendered-size, 9rem) - 1rem);

				font-size: 16px;
				font-weight: bold;
				padding: .25rem 0;
			}

			.label .required {
				color: #900 !important;
			}

			@media screen and (min-width: 768px) {
				.row { --flex-dir: row }
				.label {
					--rendered-size: 9rem;
					--flex-algin: flex-end;
				}
			}


			.field {
				border: none;
				outline: none;
				
				font-size: 16px;
				line-height: 1.5;

				width: 100%;
				padding: .5rem 1rem;

				box-sizing: border-box;
				transition: .15s ease-in-out box-shadow;

				box-shadow: 0 0 3px 0px var(--border);
				background-color: var(--background);
			}

			.field:focus { box-shadow: 0 0 5px 2px var(--border) }
			.field.error {
				--border: #9006;
				--background: #99000006;
			}
			.field.valid {
				--border: #0906;
				--background: #00990006;
			}

			@media screen and (min-width: 768px) {
				.field { margin-left: 1rem }
			}
		`;
	}

	get __fieldElem() {
		return html`
			<input class=${this.__fieldClasses}
				.value=${this.value || ''}
				type=${this.__fieldType}>
		`
	}

	get __requiredFlag() {
		if (!this.isRequired) return;
		return html`<span class="required">*</span>`
	}

	render() {
		return html`
			<div class="row">
				<div class="label">
					<span>
						${this.__requiredFlag}
						${this.label}
					</span>	
				</div>

				${ this.__fieldElem }
			</div>
		`
	}
}

customElements.define('lit-text-field', LitTextField);
