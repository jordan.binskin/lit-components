
import { html, css } from 'lit-extended';
import { LitTextField } from '../lit-text-field/lit-text-field'

export class LitSelectField extends LitTextField {
	constructor() {
		super();
		this.required = true;
		this.isValid = false;

		this.__children = Array.from(this.children);
		this.__options = this.__children.filter(child => child instanceof HTMLOptionElement);
	}

	static get properties() {
		return {
			options: {
				type: Array,
				reflect: true
			}
		}
	}

	static get styles() {
		return css`
			${LitTextField.styles}
			
            .field {
				background-image: url("http://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png");
				background-position: calc(100% - 1rem) center;
				background-repeat: no-repeat;
				
				line-height:1.5;
				width: 100%;
				box-sizing: border-box;
				-webkit-appearance: none;
				border-radius: 0px;
			}
			
			.field .option {

			}

			@media screen and (min-width: 768px) {
				:host span {
					text-align: right;
				}
			}
		`;
	}

	get value() {
		return this.__renderedField.options[this.__renderedField.selectedIndex].getAttribute('value');
	}
	set value(val) {
		this.__renderedField.value = val;
		this.__validate();
	}



	get __hasValue() {
		return !!this.value && this.value.trim() !== '';
	}

	get __renderedField() {
		return this.query('select');
	}

	get __fieldElem() {
		return html`
			<select class="${this.__fieldClasses}"
				@change=${() => this.__validate()}>
				${ html`${this.__options}` }
			</select>
		`
	}
}


customElements.define('lit-select-field', LitSelectField);
